![Logo](images/OL_BANNER.png)

Acest document și în alte limbi:

|[english](README.md)|[català](README-ca.md)|[deutsch](README-de.md)|[español](README-es.md)|[français](README-fr.md)|[日本語](README-ja.md)|[nederlands](README-nl.md)|[polski](README-pl.md)|[português](README-pt_BR.md)|[Русский](README-ru.md)|[svenska](README-sv.md)|[türkçe](README-tr.md)[汉语](README-zh-Hans.md)|[漢語](README-zh-Hant.md)
|---|---|---|---|---|---|---|---|---|

# Low Resource Bag Valve Mask (BVM) Ventilator

- Acest proiect a fost pornit datorită pandemiei globale COVID-19 (SARS-CoV-2) în urma unor discuții ale comunității pe un grup de facebook numit Open Source COVID19 și OpenSourceVentilator. Din acest motiv am creat acest proiect GitLab pentru un nou produs open source ce se numește **OpenLung** („PlămânulDeschis”).
- Mai specific, într-o discuție privind un respirator de urgență de preț redus (low cost) bazat pe **Bag Valve Mask** (**BVM** sau **AmbuBag**) unde soluțiile anterioare au fost construite. [Primul respirator venit din partea unui grup de cercetare MIT](https://web.mit.edu/2.75/projects/DMD_2010_Al_Husseini.pdf) compus din persoanele urmtăoare: Abdul Mohsen Al Husseini, Heon Ju Lee, Justin Negrete, Stephen Powelson, Amelia Servi, Alexander Slocum and Jussi Saukkonen. [Al doilea dispozitiv a venit de la un grup de studenți de la Universitatea de Mecanică și Inginerie Rice (Rice University Mechanical Engineering)](http://oedk.rice.edu/Sys/PublicProfile/47585242/1063096) compus din persoanele următoare: Madison Nasteff, Carolina De Santiago, Aravind Sundaramraj, Natalie Dickman, Tim Nonet and Karen Vasquez Ruiz.
- Acest proiect are obiectivul de a combina și îmbunătății eforturile acestor două proiecte (mai sus menționate) într-un dispozitiv mai simplu și mai sigur, dispozitiv ce constă din părți ușor de procurat sau de printat 3D.
- Beneficii: Poate fi produs în masă, punctele de contactat folosesc componente certificate, cerințe mecanice puține și simple, cercetare și testare anterioare în această zonă, adaptabil la intubare și mască invazive.

*ATENȚIE: Pe cât posibil, vă rugăm să căutați ajutor medical profesional ce dispune de echipament propriu instalat de către profesioniști acreditați. Nu folosiți informații aleatorii pe care le-ați găsit pe internet. Nu suntem profesioniști medicali, doar simpli oameni de pe internet.*

# Cum să te alături Dezvoltării (Development):
1. Completează Formularul Expresiei Intenției de Interes [aici](https://opensourceventilator.ie/register) în cazul în care nu ai făcut-o deja. Dacă ai echipament de fabricație, completează și formularul respectiv (fabrication equipment).
2. Te vom contacta îndată ce aptitudinile de care dai dovadă ne vor fi necesare, până atunci verifică următoarele:
-- Rămânerea la curent cu acest proiect prin a ne vizita website-ul [Open Source Ventilators Ireland](https://opensourceventilator.ie/) și canalele noastre sociale.
-- Vino alături de noi pe [canalul nostru Slack](https://join.slack.com/t/osventilator/shared_invite/zt-cst4dhk7-BFNMz_vyBPthjlBFYV1yWA) pentru a participa la conversații. Multe alte proiecte își au locul acolo.
3. Familiarizează-te cu git, învață cum să contribui cel mai bine la acest repository. Sunt foarte multe resurse disponibile online, poți începe cu [YouTube: Introducere la Workflow-ul GitLab (eng.)](https://www.youtube.com/watch?v=enMumwvLAug) sau învață cum să trimiți un „merge request” citind [documentația GitLab (eng.)](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).
4. Dacă ești nerăbdător/nerăbdătoare și îți dorești să începi să dezvolți imediat în acest proiect de tip open source, atunci te rugăm să faci fork la acest repo și să te apuci imediat!
*Dacă începi să contribui înainte de a te contacta, te rugăm să urmezi guideline-ul pentru aducerea de contribuții așa cum este descris [aici (eng.)](https://gitlab.com/TrevorSmale/OSV-OpenLung/-/blob/master/CONTRIBUTING.md).*

# Cerințele Proiectului
- Cerințele Proiectului sunt descrise [aici (eng.)](requirements/design-requirements.md).

# Progresul Proiectului
![Current Mechanical Concept](images/CONCEPT_6_MECH.png)
*Concept-ul design current 6 cu problemele cunoscute*